# STM32F0 Manual

A short hands-on introduction to get started developing applications for STM32 microcontrollers using the STM32CubeIDE.

The HAL (Hardware Abstraction Layer) is very well documented. For additional info feel free to go to the definitions of the specific functions and variables you want to look at (F3 or context menu).
For technical details regarding the architecture refer to the vast number of reports and manuals that STM provides (probably stored in the Sync&Share somewhere).

Also look into the **Snippet** section! Some useful code-bits to get you going with more advanced topics are collected there. 

## Creating the first project

As a starter we want to create a program that lights up a LED when the blue button on the board is pressed. This should give you some insight into the basic workflow. 

- Select the correct board (STM32F072B-Discovery) otherwise drivers are eventually incompatible:
  ![](Gifs/new_project.gif)

- This leaves you with a graphical device configuration tool. Each pin resembles the actual pin on your board that you can use as analog or digital GPIO in- and output, timer inputs or as interfaces to connect external devices such as displays. By selecting a mode for a specific pin and saving (Ctrl+s) the IDE generates code automatically to initialize the right components on the software level.


### Configuration of the components

- PA0 is connected to the blue pushbutton and PC7 to the one of the 4 LEDs on the board (LD6)
- Set PA0 as a GPIO input and PC7 as GPIO output. You can label them, the will be respected as prefixes for several macro definitions.
  ![](Gifs/button_led_pinout.gif)
- Make sure you save in order to generate the necessary code!

### Programming

Since we are programming in C (or C++ if you like) you can organize your code in the typical fashion: Definitions go to the header files and implementations to source files.
For now we want to look at the main source file (`main.c` in the `~/Src` directory).
- **IMPORTANT**: Only write your own code in the user code blocks! Otherwise it will be overwritten when altering something in the graphical pinout view!
  ~~~c
  /* USER CODE BEGIN XY */
  // nice code...
  /* USER CODE END XY */
  ~~~

- After recovering from the shock you suffered when you first saw the code you move to the main function.

- Insert the following into the endless loop in the main function (it should be self-explanatory):
  ~~~c
  while (1)
  {
      if (HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin)) {
          HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
      }
      else {
          HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
      }
  }
  ~~~

- Connect your board to the PC and press F11 to start compiling and flashing your microcontroller. 
  ![](Gifs/flash_device.gif)

- Make sure that you use the ST-LINK debugger. When there is a breakpoint stopping the programm, press F8 to resume.
  Try to press the button, the LED should light up now. You can stop the debugging process if you wish, the program is stored on the board.
